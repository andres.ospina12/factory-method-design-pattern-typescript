import { Logistics } from '../Logistics';



export abstract class Creator {
    public abstract factoryMethod(): Logistics;

    public someOperation(): string {
        const product = this.factoryMethod();
        return `Creator: The same creator's code has just worked with ${product.operation()}`
    }
}
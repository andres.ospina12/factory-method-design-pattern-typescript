import { Logistics } from "../Logistics";
import { RoadLogistics } from "../RoadLogistics";
import { Creator } from "./Creator";

export class RoadLogisticsCreator extends Creator {
    public factoryMethod(): Logistics {
        return new RoadLogistics();
    }
}
import { Logistics } from "../Logistics";
import { Creator } from "./Creator";
import { SeaLogistics } from '../SeaLogistics';

export class SeaLogisticsCreator extends Creator {
    public factoryMethod(): Logistics {
        return new SeaLogistics();
    }
}
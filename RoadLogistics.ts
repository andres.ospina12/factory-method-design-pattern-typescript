import { Logistics } from "./Logistics";

export class RoadLogistics implements Logistics {
    public operation(): string {
        return '{¡RoadLogistics Operation!}';
    }
}
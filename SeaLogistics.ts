import { Logistics } from "./Logistics";

export class SeaLogistics implements Logistics {
    public operation(): string {
        return '{¡SeaLogistics Operation! }';
    }
}
import { Creator } from "./Creator/Creator";
import { RoadLogisticsCreator } from "./Creator/RoadLogisticsCreator";
import { SeaLogisticsCreator } from "./Creator/SeaLogisticsCreator";

const clientCode = (creator: Creator) => {
    // ...
    console.log(creator.someOperation());
    // ...
}




console.log('App: Launched with the SeaLogisticsCreator.');
clientCode(new SeaLogisticsCreator());

console.log('App: Launched with the RoadLogisticsCreator.');
clientCode(new RoadLogisticsCreator());